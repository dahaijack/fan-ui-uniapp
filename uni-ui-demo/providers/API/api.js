export default {
	// 存放接口路径
	login: '/app-api/member/auth/weixin-mini-app-login', //登录接口
	getUserInfo: '/app-api/member/user/get', //获得用户基本信息
	updateAvatar: '/app-api/member/user/update-avatar', //修改用户头像
	updateNickname: '/app-api/member/user/update-nickname', //修改用户昵称

	createSuggestion: '/app-api/pz/suggestion/create', //创建投诉
	getServiceManage: '/app-api/pz/service-manage/get', //获得陪诊服务管理
	getServiceList: '/app-api/pz/service-manage/page', //获得陪诊服务管理分页 
	getcategoryList: '/app-api/pz/category/list', //获得服务分类列表


	bannerList: '/magic/web/app/pz/banner/list', //首页轮播
	cateGory: '/app-api/pz/category/list', //分类	
	getContentTypeList: '/magic/web/app/pz/content-type/list',
	getContentList: '/magic/web/app/pz/content/list', //常见问题列表
	getPrivacyAgreement: '/magic/web/app/pz/content/detail', //隐私协议
	getNoticeMessages: '/magic/web/app/pz/notice/list', //消息列表
	getOrderList: '/magic/web/app/pz/order/reservation/list', //订单列表
	submitOrder: '/app-api/pay/order/submit', //提交订单
	getPayParams: '/app-api/pay/order-reservation/submit', //获取支付参数

	createAddress: '/app-api/member/address/create', //新增收件地址
	updateAddress: '/app-api/member/address/update', //编辑收件地址
	delAddress: '/app-api/member/address/delete', //删除收件地址
	getAddressList: '/app-api/member/address/list', //获取收件地址集合

	addArchives: '/magic/web/app/pz/card-recipient/add', //增加档案
	addMerchants: '/magic/web/app/pay/merchant/apply' ,//商家入驻
	
	searchCaregivers:'/magic/web/app/pz/card-recipient/page',//magic/web/app/pz/card-recipient/page
	
	addReservation:'/magic/web/app/pay/order/reservation/add',//新增预约支付订单
	
	getHomeMenus: '/magic/web/app/pz/icon/list',    //获取首页菜单
	
	getMerchant: '/magic/web/app/pz/service/getmerchant/and/service', //获取商家服务
	
	merchantFollow: '/app-api/pay/merchantFollow/status',          //商户关注和取消
	getMiniBpmForm:'/app-api/mini/form/getMiniBpmForm',//小程序获得动态表单
	
	getMerchantById: '/magic/web/app/pay/merchant/get'   //查询商户信息
	
}
