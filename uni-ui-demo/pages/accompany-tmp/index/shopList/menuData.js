export const menuData = [
	 { name: 'whole', title: '全部', options: [
	 	{ label: "全部", value: null },
	 	{ label: "平台精选", value: "1" },
	 	{ label: "企业店铺", value: "2" },
	 	{ label: "个人小店", value: "3" },
	 ] },
	 { name: 'services', title: '全部服务', treeSelect: true,options: [
		{ label: "全部服务", value: null, children: [{ label: '全部', value: null }] },
		{ label: "护士上门", value: "1", 
			children: [ 
				{ label: "全部", value: null},
				{ label: "打针采血",value: "1_1"},
				{ label: "上门换药",  value: "1_2"},
			],
		},
		{ label: "陪诊服务", value: "2",
			children: [ 
				{ label: "全部", value: null},
				{ label: "护士陪诊", value: "2_1"},
				{ label: "院内陪诊", value: "2_2"},
			],
		},
	] },
	
	{ name: 'sort', title: '排序',options: [
	 	{ label: "全部", value: null },
	 	{ label: "距离优先", value: "1" },
	 	{ label: "好评优先", value: "2" },
	 ]}
]