
export const templateName = 'accompany'

// 首页菜单导航
const icon1 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav1.png';
const icon2 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav2.png';
const icon3 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav3.png';
const icon4 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav4.png';
const icon5 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav5.png';
const icon6 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav6.png';
const icon7 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav7.png';
const icon8 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/home/nav8.png';

export const navList = [
	{ icon: icon1, label: '护士上门', path: '' },
	{ icon: icon2, label: '居家照护', path: '' },
	{ icon: icon3, label: '医院陪护', path: '' },
	{ icon: icon4, label: '陪诊陪检', path: '' },
	{ icon: icon5, label: '母婴服务', path: '' },
	{ icon: icon6, label: '康复服务', path: '' },
	{ icon: icon7, label: '辅助挂号', path: '' },
	{ icon: icon8, label: '更多服务', path: '/pages/accompany-tmp/index/shopList/shopList' }
]

// 我的页面列表导航
const myIcon1 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav1.png';
const myIcon2 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav2.png';
const myIcon3 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav3.png';
const myIcon4 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav4.png';
const myIcon5 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav5.png';
const myIcon6 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav6.png';
const myIcon7 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav7.png';
const myIcon8 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav8.png';
// const myIcon9 = 'http://jiaoyukehu.oss-cn-beijing.aliyuncs.com/static/my/nav9.png';

export const myNavList = [
	{ icon: myIcon1, label: '健康档案', left: '可添加5人', right: '新建档案', path: '', w: 40, h: 40 },
	{ icon: myIcon2, label: '关注店铺', path: '/pages/accompany-tmp/my/myAttention/myAttention', w: 40, h: 38, border: true },
	{ icon: myIcon3, label: '地址管理', path: '/pages/accompany-tmp/my/addressList/addressList', w: 34, h: 44, border: true },
	{ icon: myIcon4, label: '联系客服', type:'contact', w: 40, h: 40, border: true },
	{ icon: myIcon5, label: '常见问题', path: '/pages/accompany-tmp/index/question/question', w: 40, h: 44, border: true },
	{ icon: myIcon6, label: '关注公众号', path: '', w: 46, h: 40, border: true },
	{ icon: myIcon7, label: '关于我们', path: '/pages/accompany-tmp/my/privacyAgreement/privacyAgreement?id='+5, w: 44, h: 44, border: true },
	{ icon: myIcon8, label: '商务合作', path: '/pages/accompany-tmp/my/privacyAgreement/privacyAgreement?id='+6, w: 48, h: 40 },
	{ icon: myIcon6, label: '商家申请入驻', path: '/pages/accompany-tmp/my/myMerchantsApply/myMerchantsApply', w: 48, h: 40 }
	// { icon: myIcon2, label: '培护师申请', path: '/pages/accompany-tmp/my/myTrainerApplication/myTrainerApplication', w: 48, h: 40 }

]
