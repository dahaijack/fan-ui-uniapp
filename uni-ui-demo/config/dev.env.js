/**
 * 开发环境配置文件
 * @description 
 * import DEV_CONFIG from '@/config/dev.env.js';
 */

export default {
	ENV: 'DEV',
	AppVersion: '1.0.0', 
	DEBUG: true, 
	SERVER_URL: 'https://scrm.caomei.zone/', // 开发环境地址
	SERVER_URL_FIN: '', // 测试环境地址
	PATH: '',
	PRIVATE_KEY: '',
	PUBLIC_KEY: ''
}
