export const dropdownMenuProps = {
	value: {
		type: Object,
		default: () => {}
	},
	data: {
		type: Array,
		required: true,
		default: () => []
	},
	activeColor: {
		type: String,
		default: '#1989fa'
	},
	zIndex: {
		type: Number,
		default: 10
	},
	overlay: {
		type: Boolean,
		default: true
	},
	closeOnClickOverlay: {
		type: Boolean,
		default: true
	},
	duration: {
		type: Number,
		default: 0.3
	},
	menuItemStyle: {
		type: Object,
		default: {},
	},
	popupSytle: {
		type: Object,
		default: {},
	}
}
