import App from './App'
import Http from '@/providers/essential/httpService.js'
import api from '@/providers/API/api.js'
import share from '@/mixins/share.js';

import DEV_CONFIG from '@/config/dev.env.js';

const ENV_CONFIG = DEV_CONFIG

// #ifndef VUE3
import Vue from 'vue'


Vue.config.productionTip = false
App.mpType = 'app'

try {
  function isPromise(obj) {
    return (
      !!obj &&
      (typeof obj === "object" || typeof obj === "function") &&
      typeof obj.then === "function"
    );
  }

  // 统一 vue2 API Promise 化返回格式与 vue3 保持一致
  uni.addInterceptor({
    returnValue(res) {
      if (!isPromise(res)) {
        return res;
      }
      return new Promise((resolve, reject) => {
        res.then((res) => {
          if (res[0]) {
            reject(res[0]);
          } else {
            resolve(res[1]);
          }
        });
      });
    },
  });
} catch (error) { }

const app = new Vue({
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  // 全局属性配置
  app.config.globalProperties.$UNIHTTP = Http  //post请求
  app.config.globalProperties.$API = api  //api列表
  app.config.globalProperties.$config = ENV_CONFIG //开发配置
  app.mixin(share);
  return {
    app
  }
}
// #endif